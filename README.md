# README #

Welcome to the repository for trained networks of the paper "Inverse Design of Self-Oscillating Gels Through Deep Learning".
This repository has the source codes for training the neural networks mentioned in the publication. The inverse design architecture mainly relies on PyTorch machine learning library (more dependencies can be found in the respective files). All of the trained networks are not included in this repository due to storage constraints, for all networks trained under this study please visit [this](https://drive.google.com/drive/folders/1b9UgheQQ5ZEUXoZf1LWFXYp4zd4arXGO?usp=sharing) Google Drive 


### Contents ###

* Classification
* Regression

### Classification ###
Networks trained using the PyTorch based classifier ["CatalystGels_Regression0_1second_fbfs_DoubleMLP_pytorch.py"](https://bitbucket.org/dorukaks/inverse_design_of_self_oscillating_gels_through_deep_learning/src/src/Regression/CatalystGels_Regression0_1second_fbfs_DoubleMLP_pytorch.py) are stored here. The name of the files are in "MLPClassifier_pytorch_[NumberofInstances]_train[TrainTime]s_pc[NumberofPCs]_[NumberofLayers]layers_[HiddenUnits].pth"
* NumberofInstances: Number of simulation snapshots used in training
* TrainTime: Time of the snapshot used in training (for 10 instances this identifier signifies the first snapshot of the 10 snapshot series)
* NumberofPCs: Number of the principal components fed into the classifier after coordinate preprocessing
* NumberofLayers: Number of hidden layers in the MLP classifier
* HiddenUnits: Number of hidden units in each hidden layer in the MLP classifier

### Regression ###
Networks trained using the PyTorch based regressor ["CatalystGels_Regression0_1second_fbfs_DoubleMLP_pytorch.py"](https://bitbucket.org/dorukaks/inverse_design_of_self_oscillating_gels_through_deep_learning/src/src/Regression/CatalystGels_Regression0_1second_fbfs_DoubleMLP_pytorch.py) are stored here. The name of the files are in "MLPRegressor_[Coordinate1][Coordinate2]_[NumberofForceTerms]fbfs_[Preprocessing][Number]_[split/nosplit]_[Epochs1][Epochs2]_[PreprocessingEpochs]_[FirstMLPLayer][SecondMLPLayer]_[FirstMLPHiddenUnits][SecondMLPHiddenUnits]_DoubleMLP.pth"

* Coordinate1: First coordinate component of the lattice gel used in training
* Coordinate2: Second coordinate component of the lattice gel used in training
* NumberofForceTerms: Number of force components (for each term) of the lattice gel used in training
* Preprocessing: Type of preprocessing used on coordinate components
* Number: Number of coordinate components (for each coordinate) fed into the regressor after coordinate preprocessing
* split/nosplit: Preprocessing type of the coordinate componets (if split, each coordinate component is preprocessed separately)
* Epochs1: Number of epochs used to train the first MLP regressor
* Epochs2: Number of epochs used to train the second MLP regressor
* PreprocessingEpochs: Number of epochs used to train the preprocessor (applicable if preprocessor is AutoEncoder)
* FirstMLPLayer: Number of hidden layers in the first MLP regressor
* SecondMLPLayer: Number of hidden layers in the second MLP regressor
* FirstMLPHiddenUnits: Number of hidden units in each hidden layer in the first MLP regressor
* SecondMLPHiddenUnits: Number of hidden units in each hidden layer in the second MLP regressor

### Related Repositories ###

* [Main Files](https://bitbucket.org/dorukaks/inverse_design_of_self_oscillating_gels_through_deep_learning/src/src/) - The networks trained using the presented architecture can be found here. The networks and the auxillary preprocessors (such as PCA and scalers) are packed/saved using PyTorch module.
* [Result Files](https://bitbucket.org/dorukaks/result_files/src/main/) - Statistics for the trained networks are provided here. The stats are packed/saved using pickle library.

* For further questions regarding this work, please contact to owner of this repository  (Doruk Aksoy)
* [Computational Autonomy Group](http://www.alexgorodetsky.com/index.html)



Copyright © 2020-2021 Doruk Aksoy

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
